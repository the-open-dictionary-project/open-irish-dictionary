package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.irish.IrishLenition;
import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

public abstract class LenitedIrishVerbTense extends IrishVerbTense {

    protected final boolean hasLenition;

    public LenitedIrishVerbTense(IrishVerb verb, JSONObject obj, boolean defaultHasLenition){
        super(verb, obj);
        hasLenition = obj.optBoolean("hasLenition", defaultHasLenition);
    }

    public LenitedIrishVerbTense(IrishVerb verb, JSONObject obj) {
        this(verb, obj, true);
    }

    protected String applyBroadOrSlenderWithoutLenition(String broad, String slender){
        return super.applyBroadOrSlender(broad, slender);
    }

    @Override
    protected String applyBroadOrSlender(String broad, String slender) {
        if(hasLenition)
            return IrishLenition.preformLenition(super.applyBroadOrSlender(broad, slender));
        else
            return super.applyBroadOrSlender(broad, slender);
    }

}
