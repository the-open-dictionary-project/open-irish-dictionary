package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.common.Conjugation;
import cymru.prv.dictionary.common.json.Json;
import cymru.prv.dictionary.irish.IrishLenition;
import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

import java.util.List;

public abstract class IrishVerbTense extends Conjugation {

    private static final String DEFAULTS = "defaults";
    private static final String ANALYTIC = "analytic";

    protected final List<String> analytic;
    protected List<String> defaults;
    protected final String stem;

    protected abstract List<String> getDefaultAnalytic();

    public IrishVerbTense(IrishVerb verb, JSONObject obj) {
        super(obj);
        stem = obj.optString("stem", verb.getStem());
        if(obj.has(ANALYTIC))
            analytic = Json.getStringList(obj, ANALYTIC);
        else
            analytic = null;

        if(obj.has(DEFAULTS))
            defaults = Json.getStringList(obj, DEFAULTS);
        else
            defaults = null;
    }

    @Override
    public JSONObject toJson() {
        JSONObject obj = super.toJson();

        if(analytic != null)
            obj.put(ANALYTIC, analytic);

        List<String> defaultAnalytic = getDefaultAnalytic();
        if(defaultAnalytic != null)
            obj.put(ANALYTIC, analytic);

        return obj;
    }

    protected String apply(String suffix) {
        if(stem.endsWith("í") && suffix.startsWith("i"))
            return stem + suffix.substring(1);
        if(stem.endsWith("á") && suffix.startsWith("a"))
            return stem + suffix.substring(1);
        if(stem.endsWith("é") && suffix.startsWith("e"))
            return stem + suffix.substring(1);
        if(stem.endsWith("" + suffix.charAt(0)))
            return stem + suffix.substring(1);
        return stem + suffix;
    }

    protected String applyBroadOrSlender(String broad, String slender) {
        if(IrishLenition.isBroad(stem))
            return apply(broad);
        else
            return apply(slender);
    }


    protected List<String> getDefaultsOrAnalytic(){
        if(defaults != null)
            return defaults;
        return getDefaultAnalytic();
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        if(analytic != null)
            list.addAll(analytic);
        else {
            var analyticDefaults = getDefaultAnalytic();
            if(analyticDefaults != null)
                list.addAll(analyticDefaults);
        }
        return list;
    }
}
