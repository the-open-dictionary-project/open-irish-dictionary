package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SecondImperfectVerbTense extends LenitedIrishVerbTense {

    public SecondImperfectVerbTense(IrishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("aíodh","íodh"));
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(applyBroadOrSlender("aínn", "ínn"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(applyBroadOrSlender("aíteá", "íteá"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyBroadOrSlender("aímis", "ímis"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.singletonList(applyBroadOrSlender("aídís", "ídís"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("aítí", "ítí"));
    }
}
