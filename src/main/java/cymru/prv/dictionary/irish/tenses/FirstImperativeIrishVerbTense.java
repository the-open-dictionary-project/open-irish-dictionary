package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class FirstImperativeIrishVerbTense extends IrishVerbTense {

    public FirstImperativeIrishVerbTense(IrishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultAnalytic() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(stem);
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Collections.singletonList(applyBroadOrSlender("aigí", "igí"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.emptyList();
    }
}
