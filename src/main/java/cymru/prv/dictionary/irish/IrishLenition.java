package cymru.prv.dictionary.irish;

import java.util.Arrays;
import java.util.Collections;


/**
 * A helper class to help with lenition in Irish
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public final class IrishLenition {

    // Private constructor to prevent initialisation
    private IrishLenition(){}

    public static String preformLenition(String word){
        word = word
                .replaceFirst("^p", "ph")
                .replaceFirst("^t", "th")
                .replaceFirst("^c", "ch")
                .replaceFirst("^b", "bh")
                .replaceFirst("^d", "dh")
                .replaceFirst("^g", "gh")
                .replaceFirst("^m", "mh")
                .replaceFirst("^s(?![cmnlpr])", "sh")
                .replaceFirst("^f", "fh");
        if(word.matches("^(a|e|u|i|o|fh).*") && !word.matches("^(ais|ath).*"))
            word = "d'" + word;
        return word;
    }

    public static String removeLenition(String word){
        return word
                .replaceFirst("d'", "")
                .replaceFirst("^ph", "p")
                .replaceFirst("^th", "t")
                .replaceFirst("^ch", "c")
                .replaceFirst("^bh", "b")
                .replaceFirst("^dh", "d")
                .replaceFirst("^gh", "g")
                .replaceFirst("^mh", "m")
                .replaceFirst("^sh", "s")
                .replaceFirst("^fh", "f");
    }

    public static boolean isBroad(String word){
        int lastBroad = Collections.max(Arrays.asList(
                word.lastIndexOf("a"),
                word.lastIndexOf("á"),
                word.lastIndexOf("u"),
                word.lastIndexOf("ú"),
                word.lastIndexOf("o"),
                word.lastIndexOf("ó")
        ));
        int lastSlender = Collections.max(Arrays.asList(
                word.lastIndexOf("e"),
                word.lastIndexOf("é"),
                word.lastIndexOf("i"),
                word.lastIndexOf("í")
        ));
        return lastBroad > lastSlender;
    }

}
